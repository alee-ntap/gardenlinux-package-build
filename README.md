# Garden Linux package build

# Garden Linux repository suites

The suite `today` and the `major.0` suites are created daily. Patch releases `major.minor` with minor > 0 must be created manually. Please find a detailed documentation [here](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/tree/main/packages)

## Pipelines

### Normal

How to use:

```yaml
include:
  project: gardenlinux/gardenlinux-package-build
  file: pipeline/pipeline.yml
```

### Native package

How to use:

```yaml
include:
  project: gardenlinux/gardenlinux-package-build
  file: pipeline/pipeline-native.yml
```

## Variables

Some parts of the build jobs can be modified with variables.

Variable | Description | Default
|---|---|---|
`BUILD_ARCH_ALL` | Build package for "all" architecture. | False
`BUILD_ARCH_AMD64` | Build package for "amd64" architecture. | True
`BUILD_ARCH_ARM64` | Build package for "arm64" architecture. | True
`BUILD_SIGNED` | Build packages using the Debian secure boot signing protocol. | False

## Runner tags

Some runner need to expose pre-defined tags.

* `gardenlinux-build-amd64`: Native builder for `amd64`
* `gardenlinux-build-arm64`: Native builder for `arm64`
* `gardenlinux-repository`: Runner for repository access

## Versioning

Garden Linux packages which are built by this pipeline are either versioned based on the source package provided by the configured distribution (e.g. `debian:trixie`) or the version provided by the corresponding Git tag of the package project.

In both cases, the Debian version of the package defines the general Garden Linux package version. It is then extended by a Garden Linux specific suffix in order to distinguish Garden Linux packages from their original Debian packages. More information about how the Debian versioning looks like and of which components it consists of, can be found in the [Debian Policy](https://www.debian.org/doc/debian-policy/ch-controlfields.html#version). 

Garden Linux packages are built each time a commit is pushed to the main branch or to a pull request of the corresponding package project.
If a commit is related to a Git tag, a developer wants to release the corresponding Garden Linux package by providing a stable package version that does not change afterwards.

Typically, two methods are used to assign a version to a Garden Linux package:

### Push without Git Tag

If a commit is NOT related to a Git tag, the version of the Garden Linux package is determined by the version of its source package. Thereby, the distribution of the source package is defined by the variable `SOURCE_DIST` which currently defaults to `trixie`.

For this reason, the Garden Linux package is based on the package version of the corresponding package in `trixie` and would mainly look like this then:

#### Update in a pull request
```
<Debian version>gardenlinux~<CI Merge Request IID>.<CI Pipeline ID>.<CI COMMIT SHORT SHA>
0.0.1-1gardenlinux~5.519384992.d9028c7b
```

#### Update in the main branch
```
<Debian version>gardenlinux~0.<CI Pipeline ID>.<CI COMMIT SHORT SHA>
0.0.1-1gardenlinux~0.519384992.d9028c7b
```

### Push with Git tags
If a commit is related to a Git tag however, the Git tag defines the whole package version. Thus, it must have the following structure to fit the general version schema:
```
gardenlinux/<Debian version>gardenlinux<count>
gardenlinux/2.1.27+dfsg2-3gardenlinux1
```
Here, the count must be increased each time a Git tag already exists for the given Debian version. This allows us to release multiple Garden Linux packages for the same Debian version. Since, the tag defines how the Garden Linux package version will look like, it must be unique.

The pipeline will then remove the `gardenlinux/` prefix of the Git tag and ensure that the rest of the tag will be used as the version of the package.

Once, the Debian version also contains an epoch version part ([Debian Policy](https://www.debian.org/doc/debian-policy/ch-controlfields.html#version)), the Git tag must follow the following pattern:
```
gardenlinux/<Debian epoch version>%<Debian version>gardenlinux<count>
gardenlinux/1%9.18.0-2gardenlinux1
```
Git does not allow to use colons within tags. For this reason, the percent symbol is used as an alternative.

It is replaced with a colon as soon as the build will be executed to enforce a correct version format during the build.
