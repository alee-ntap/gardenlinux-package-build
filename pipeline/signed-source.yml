.signed source:
  stage: signed source
  image: debian:trixie-slim
  script:
  # Real script is specified inside the GitLab runner.
  - 'false'
  variables:
    GARDENLINUX_JOB_NAME: sign
  tags:
  - gardenlinux-repository
  artifacts:
    paths:
    - _output/*.changes
    - _output/*.dsc
    - _output/*.tar.*
    expire_in: 2 days

.signed source dev:
  stage: signed source
  image: debian:trixie-slim

  before_script:
  - >
    if [[ $CI_DISPOSABLE_ENVIRONMENT ]]; then
      apt-get update -qy
      apt-get install -qy --no-install-recommends dpkg-dev openssl jq pesign
    fi

  script:
  - INPUT_DIR=_input
  - OUTPUT_DIR=_output
  - mv $OUTPUT_DIR $INPUT_DIR
  - |
    mkdir -p ${OUTPUT_DIR}/tmp
    echo '### create nss cert store'
    openssl pkcs12 -export -out ${OUTPUT_DIR}/tmp/sign.p12 -inkey $SIGNING_KEY_FILE -in $SIGNING_KEY_FILE -passout pass:"" -name sign
    pk12util -i ${OUTPUT_DIR}/tmp/sign.p12 -d ${OUTPUT_DIR}/tmp -W "" -K "" >/dev/null

    for template in ${INPUT_DIR}/*-signed-template_*_*.deb; do
      template_deb=$(basename $template)
      template_name=${template_deb%%_*}
      template_dir=${OUTPUT_DIR}/${template_name}
      template_tmpdir=${OUTPUT_DIR}/tmp/${template_name}
      template_json=${OUTPUT_DIR}/tmp/${template_name}.json

      echo "### extracting template $template_name"
      dpkg-deb -x $template ${template_tmpdir}
      mv ${template_tmpdir}/usr/share/code-signing/${template_name}/files.json ${template_json}
      mv ${template_tmpdir}/usr/share/code-signing/${template_name}/source-template ${template_dir}

      for unsigned_name in $(jq -j '.packages | keys | join(" ")' < ${template_json}); do
        unsigned_dir=${OUTPUT_DIR}/tmp/${unsigned_name}

        echo "### extracting unsigned package $unsigned_name"
        dpkg-deb -x ${INPUT_DIR}/${unsigned_name}_*_*.deb $unsigned_dir

        echo "### signing package $unsigned_name"
        while read -r file sigtype; do
          filein=$unsigned_dir/$file
          fileout=${template_dir}/debian/signatures/${unsigned_name}/${file}.sig
          dirout=$(dirname $fileout)

          mkdir -p $dirout
          if [[ $sigtype = efi ]]; then
            pesign -i $filein --export-signature $fileout --sign -d sha256 -n ${OUTPUT_DIR}/tmp -c sign
          elif [[ $sigtype = linux-module ]]; then
            openssl cms -sign -binary -outform DER -signer $SIGNING_KEY_FILE -md sha256 -nocerts -noattr -nosmimecap -in $filein -out $fileout
          else
            echo "ERROR: unknown signing type $sigtype"
            exit 1
          fi
        done < <(jq -j ".packages[\"${unsigned_name}\"].files[] | [.file, .sig_type] | join(\" \") + \"\n\"" < ${template_json})
      done

      echo "### building source package $template_name"
      ( cd ${template_dir}; dpkg-buildpackage -us -uc -S -nc -d )
    done

  artifacts:
    paths:
    - _output/*.changes
    - _output/*.dsc
    - _output/*.tar.*
    expire_in: 2 days

signed source amd64:
  extends: .signed source
  dependencies:
  - build amd64
  rules:
  # Only run for release tags "gardenlinux/*" and if the pipeline was created
  # by push, not by web interface or other types.
  - if: '$CI_COMMIT_TAG =~ /^gardenlinux\// && $CI_PIPELINE_SOURCE == "push" && $BUILD_SIGNED != "" && $BUILD_ARCH_AMD64 != ""'

signed source amd64 dev:
  extends: .signed source dev
  dependencies:
  - build amd64
  rules:
  - if: '$CI_COMMIT_TAG !~ /^gardenlinux\// && $BUILD_SIGNED != "" && $BUILD_ARCH_AMD64 != ""'

signed source arm64:
  extends: .signed source
  dependencies:
  - build arm64
  rules:
  # Only run for release tags "gardenlinux/*" and if the pipeline was created
  # by push, not by web interface or other types.
  - if: '$CI_COMMIT_TAG =~ /^gardenlinux\// && $CI_PIPELINE_SOURCE == "push" && $BUILD_SIGNED != "" && $BUILD_ARCH_ARM64 != ""'

signed source arm64 dev:
  extends: .signed source dev
  dependencies:
  - build arm64
  rules:
  - if: '$CI_COMMIT_TAG !~ /^gardenlinux\// && $BUILD_SIGNED != "" && $BUILD_ARCH_ARM64 != ""'
