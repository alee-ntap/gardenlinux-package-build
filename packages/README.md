# Package configuration

## Table of Content
- [General](#general)
- [Configuration file structure](#configuration-file-structure)
  - [Section: Publish](#section-publish)
  - [Section: Mirrors](#section-mirrors)
- [Package Filter](#package-filter)
- [Package Precedence](#package-precedence)
- [Howtos](#howtos)
  - [Freeze release](#freeze-release)
  - [Remove packages](#remove-packages)

## General
This documentation explains the purpose of the packages configuration files within the [packages/](packages/) directory. Each file is written in YAML format.

Thereby, the `packages/` directory contains at least a so called `today.yaml` which defines what packages are available in the most recent Garden Linux release. This is typically `today`.

In general, these configurations here specify what packages of a given Garden Linux release can be downloaded from the `repo.gardenlinux.io` repository. The repository is described in more detail [here](../docs/repo.md).

Besides of the `today.yaml`, each released version of Garden Linux also has its own configuration file like for example the `576.12.yaml`. This way, an administrator can always modify the actual packages available in an already released Garden Linux version.

The whole package management is realized by the repo management tool [aptly](https://www.aptly.info/doc/overview/). The configuration made to the files described by this documentation are translated to specific `aptly` queries and commands during the publishing process. For this reason, it's recommended to take a look in the corresponding `aptly` documentation to better understand some configuration options available here.

The next chapter explains the general structure of the package configuration files.

## Configuration file structure
The configuration file typically contains the following three main sections:

| Section | Description |
|--------|--------|
|version |This is a simple parameter that describes for which release this package configuration is by default. If needed, it can be overwritten by all tools who uses this configuration during runtime. |
|publish | Based on configurable sources, this section defines what sources are used to publish the defined version of Garden Linux into `repo.gardenlinux.io`. An administrator can either reference a mirror, a local repository or exclude specific packages which are ultimately removed from the package list once the corresponding release is going to be released. |
|mirrors | Defines a list of available mirrors, which can be used as sources for the `publish` section mentioned above. Each entry specifies what APT repository should be mirrored. By using the `packages` attribute, one can also filter what packages should be mirrored from the APT repository instead of mirroring all packages available. If not defined, all packages are mirrored.|

The configuration sections `publish` and `mirror` are explained in more detail below.

### Section: Publish
The official Garden Linux repo which can be accessed remotely via `repo.gardenlinux.io` consists of multiple distributions that represent each Garden Linux Version available. Thereby, each distribution / release is put together by multiple sources.

What sources and how much sources are used for a specific Garden Linux distribution is defined by an administrator in the corresponding package configuration file (e.g. `today.yaml`). By doing so, an administrator can choose between different types of sources.

The following source types can be used:
| Source Type | Description |
|-------------|-------------|
| Repo | An administrator can use local `aptly` repos as sources for the remote Garden Linux repo by using the `repo` source type. Typically, there is just the so called `gardenlinux` repo available that is filled with packages by other repos in this Gitlab project. An administrator can then either upload all packages of this local `aptly` repo or it can filter the packages by specific filter criteria.|
| Mirror | Next to `repo`, there is the `mirror` source type. An administrator can choose between all mirrors that have been defined in the mirrors section described [here](#section-mirrors). All packages of the defined mirror are then copied to the remote Garden Linux distribution if not explicitly excluded.
| Exclude | Finally, there is the `exclude` source. This type is no real source but ultimately it allows an administrator to exclude specific binary packages that would usually be added to the remote Garden Linux distribution by other sources defined in the given configuration file. |

In addition, each source is defined by the following attributes:
* **type**: Defines the source type (Choose between `repo`, `mirror` and `exclude`)
* **name** _(optional)_: Defines the name of the source. 
  * *repo*: The name of the local repository. Repository must already exists, otherwise an error occurs.
  * *mirror*: The name of the mirror. Must match some attributes from a mirror defined in the [mirrors section](#section-mirrors). One can for example reference a specific distribution of an mirror or even an older version (snapshot) of it.
    * *Structure*: `[name]:[dist]:[version]`
    * *Example*: `debian:sid:today`
* **packages**: Defines a filter for the specific source type. Package filters are described in more detail [here](#package-filter).

### Section: Mirrors
Each package configuration file can define a list of mirrors which can be used as sources in the [publish section](#section-publish).

Thereby, an administrator needs to specify the following attributes in order to define a mirror:
* **name**: Give an mirror a unique name which can be used by the [publish section](#section-publish) to reference this mirror (e.g. `debian`).
* **url**: Defines the URL of a repository which should be mirrored (e.g. `https://deb.debian.org/debian`).
* **keyring**: Defines the keyring to verify the signature of a repo (e.g. `/usr/share/keyrings/debian-archive-keyring.gpg`).
* **arches**: Defines what package architectures should be mirrored (e.g. `[all, amd64, arm64]`).
* **dists**: Defines a list of distributions which are going to be mirrored (e.g `[trixie]`).
* **packages**: Defines a filter for packages that should be mirrored. If left empty, the whole repo is mirrored. Package filters are described in more detail [here](#package-filter). 

Usually, an administrator would not mirror a complete APT repository but use a package filter via the **packages** attribute to keep the amount of packages small. The following chapter explains in more detail what filters can be used and how they are put together.

## Package Filter

The package filter is a list of filter rules which allows to filter a set of given binary packages.

This list can for example be used to determine which package should actually be mirrored by `aptly` like it is done in the [mirrors section](#section-mirrors). Besides of this, filter rules can also be used to define what packages should be excluded or be published from a specific local repository like in the [publish section](#section-publish).

Typically, each filter rule consists of at least one filter criteria and a list of values for this criteria. In order for a binary package to be included by the list of filter rules, a given package must at least match one of the given rules. Therefore, the list of filter rules is considered to be a logical OR.

If a filter rule consists of multiple filter criteria however, the binary package must match all criteria defined by the rule. Thus, all filter criteria per rule are considered to be a logical AND.

The following criteria can be used per filter rule:

| Criteria | Description |
|--------|-------------|
| matchPriorities | Include all binary packages whose property matches the defined value. |
| matchSources | Include all binary packages whose source package is the defined value. |
| matchBinaries | Include all binary packages whose name matches the defined value. |

**NOTE**: If the package filter is used in context of a [mirror](#section-mirrors), not only the filtered binary packages are included for this specific mirror but also all binary packages, that are dependencies of the matched package are included as well.

## Package Precedence

There is a precedence between published packages defined by sources in the [publish section](#section-publish).

Packages which are defined by the source type `repo` have an higher precedence than packages referenced by an `mirror` source.

If a mirror includes a package, that is also included by a specific `repo` definition, the mirrored binary package is skipped. However, this is only the case for the binary package itself. Dependencies of the mirrored package are still published to the remote Garden Linux repo.

If a binary package is offered by multiple `repo` sources though, the last defined `repo` source wins.

## Howtos

This chapter shows typical management tasks.

### Freeze release

A freeze for a major version of Garden Linux is usually not needed because the repo management and the corresponding pipelines already create a freeze for each major version every day. The freeze is based on the `today.yaml` and the pipeline then creates two distros. The `today` distro and the major version of the corresponding day (e.g. `946.0`).

However, if a patch release is created, the distro for this patch release must be created manually. This can be achieved by freezing a specific state of packages for the patch release by providing a dedicated package definition file (YAML file) for it.

In order to freeze a given repo state for a Garden Linux release, the following steps should be done:

* Copy the `today.yaml` and named it after the release (e.g. `934.1.yaml`).
* Modify the `version` attribute in the newly created package configuration file:
  ```
  version: "934.1"
  ```
  **NOTE**:
  _Keep in mind that the version must be quoted, since YAML interprets numbers as integer or float._
* If the corresponding Garden Linux release consists of packages from a mirror, it is recommended to also freeze the state of the mirrored packages. The repo management already creates a snapshot of all defined mirrors every day. These snapshots can then be used as sources for the publishing configuration:
  ```
  - type: mirror
    name: debian:bookworm:934
  ```
* Set specific versions in the publishing sources of type `repo` to ensure that no unintended updates are executed (Keep in mind that all existing releases are updated once per day. Without setting a specific version, self built packages would be updated to the newest version available in the local repo):
  ```
  - type: repo
    name: gardenlinux
    packages:
    - matchSources:
      - cyrus-sasl2/2.1.28+dfsg-8gardenlinux1
      - dracut/057-1gardenlinux1
      - frr/8.3.1-0gardenlinux1
      - gardenlinux-selinux-module/1.0.8
      - iproute2/5.19.0-1gardenlinux1
      - linux-5.15/5.15.74-0gardenlinux1
      - linux-signed-5.15-amd64/5.15.74-0gardenlinux1
      - linux-signed-5.15-arm64/5.15.74-0gardenlinux1
      - metalbond/0.2.0-0gardenlinux3
      - microsoft-authentication-library-for-python/1.20.0-1gardenlinux1
      - onmetal-image/0.0~git20220927.d73b45a-1gardenlinux1
      - pam/1.5.2-2gardenlinux1
      - perl/5.34.0-5gardenlinux1
      - python3.10/3.10.7-1gardenlinux1
      - refpolicy/2:2.20220520-2gardenlinux1
      - selinux-basics/0.5.8garden1
      - sops/3.7.3-0gardenlinux1
      - systemd/251.5-2gardenlinux1
  ```

### Remove packages

In order to remove a package from the remote Garden Linux repo, the publishing sources must be adjusted. There are multiple ways to remove a package from the Garden Linux repo. The different actions to remove a package from the Garden Linux release are described below.

Thereby, the action described here applies to all Garden Linux releases. An administator just needs to follow one a of the following steps in the corresponding package configuration file of the release (e.g. `576.12.yaml`, `today.yaml`, ...).

* Extend the source type `exclude` by defining a package filter which matches the package that should not be included as defined by other publishing sources (e.g. `repo` or `mirror`).
* Apply a package filter per defined publishing source that excludes the package that should be removed. If a package of a local repo should not be distributed to the remote Garden Linux repo `repo.gardenlinux.io` for example, one could simply use the `packages` attribute for the given source and adjust the package filter accordingly. Usually, the list defines a list of published packages. If a corresponding package should be removed, the administrator just needs to remove it from the list.
* If a package comes from an external APT repository defined by a mirror configuration, an administrator has two options to remove an already included package:
  1. Set or modify an already existing package filter in the mirror section that excludes
     the package.
  2. Set a package filter in the publishing source type for the referenced `mirror`. Although in most cases a mirror already just contains a subset of packages of the original repository, an administrator can add an additional package filter in the `publish` section for a given mirror and exclude a package this way.