# Local testing

This is a short howto manual on how to install, setup and test the Garden Linux package repository. It is recommended to use a virtual machine for this, this example was tested on a Debian testing vagrant box.

The complete process of creating and publishing the Garden Linux repository can be found in the ```driver/run``` script.

## Install

The following packages need to be installed for a local test
```
sudo apt update
sudo apt-get install -y --no-install-recommends aptly python3 python3-pip python3-yaml git gnupg
```
To install all packages needed for the actual repository these additional packages are needed.
```
sudo apt-get install -y --no-install-recommends ca-certificates dpkg-dev dumb-init jq libnss3-tools pesign restic rsync scdaemon
```
From this repository install the shell and python scripts after checking out this repository.
```
git clone https://gitlab.com/gardenlinux/gardenlinux-package-build.git
cd gardenlinux-package-build
sudo pip install --no-deps .
```
Install [minio](https://min.io/download#/linux) to have a local s3 storage available.
```
wget https://dl.min.io/server/minio/release/linux-amd64/minio
wget https://dl.min.io/client/mc/release/linux-amd64/mc
sudo mv minio mc /usr/local/bin/
sudo chmod +x /usr/local/bin/{minio,mc}
```

## Setup
First start minio and create a bucket in the s3 storage.
```
mkdir ~/minio-data
MINIO_ROOT_USER=admin MINIO_ROOT_PASSWORD=password minio server ~/minio-data/ --console-address ":9001" &
# press <enter> to get a prompt again
mc alias set myminio http://localhost:9000 admin password
mc mb myminio/repo.gardenlinux.io/gardenlinux
```

A GPG key for signing the packages is needed, since this is for TESTING ONLY, create the key without a password.
```
gpg --gen-key 
```
Create a configuration file for aptly. NOTE: Adjust the ```rootDir``` to your needs.
```
cat <<EOF > ~/.aptly.conf
{
  "rootDir": "/home/vagrant/.aptly",
  "downloadConcurrency": 4,
  "downloadSpeedLimit": 0,
  "architectures": [],
  "dependencyFollowSuggests": false,
  "dependencyFollowRecommends": false,
  "dependencyFollowAllVariants": false,
  "dependencyFollowSource": false,
  "dependencyVerboseResolve": false,
  "gpgDisableSign": false,
  "gpgDisableVerify": false,
  "gpgProvider": "gpg",
  "downloadSourcePackages": false,
  "skipLegacyPool": true,
  "ppaDistributorID": "ubuntu",
  "ppaCodename": "",
  "skipContentsPublishing": false,
  "FileSystemPublishEndpoints": {},
  "S3PublishEndpoints": {
    "repo.gardenlinux.io": {
      "region": "us-east-1",
      "bucket": "repo.gardenlinux.io",
      "endpoint": "http://localhost:9000",
      "awsAccessKeyID": "admin",
      "awsSecretAccessKey": "password",
      "prefix": "",
      "acl": "public-read",
      "storageClass": "",
      "encryptionMethod": "",
      "plusWorkaround": false,
      "disableMultiDel": true,
      "forceSigV2": false,
      "debug": false
    }
  },
  "SwiftPublishEndpoints": {}
}
EOF
```

## Test

First setup the gardenlinux repo for aptly.
```
aptly repo create gardenlinux
```
Then the packages can be downloaded and published.
```
gardenlinux-repo-mirror-snapshot packages/today.yaml --force
gardenlinux-repo-mirror-snapshot packages/today.yaml --no-update --version-override generate
gardenlinux-repo-publish-update packages/today.yaml "s3:repo.gardenlinux.io:gardenlinux"
gardenlinux-repo-publish-update packages/today.yaml "s3:repo.gardenlinux.io:gardenlinux" --version-override generate
```

The repository can be inspected with this commands
```
aptly snapshot list
aptly repo list
aptly publish list
```
It is also possible to check the packages by looking directly into the local s3-bucket directory
```
ls ~/minio-data/repo.gardenlinux.io/
```