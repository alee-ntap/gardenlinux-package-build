# SPDX-License-Identifier: MIT

from __future__ import annotations

import argparse
import datetime
import logging
import subprocess

from gardenlinux_package_build.aptly.publish import AptlyPublish
from gardenlinux_package_build.aptly.repo import AptlyRepo
from gardenlinux_package_build.aptly.snapshot import AptlySnapshot

from .repo_publish_base import RepoPublishBase
from .utils import print_info, print_notice, print_output


class RepoPublishUpdate(RepoPublishBase):
    endpoint: str
    use_force: bool
    _snapshots_temp: set[AptlySnapshot]

    def __init__(
        self, *,
        config_file: str,
        endpoint: str,
        use_force: bool,
        timestamp_override: datetime.datetime | None = None,
        version_override: str | None = None,
    ) -> None:
        super().__init__(
            config_file=config_file,
            version_override=version_override,
        )

        self.endpoint = endpoint
        self.use_force = use_force

        self.timestamp = timestamp_override or datetime.datetime.utcnow()

    def get_repo(self, name: str) -> AptlyRepo:
        return AptlyRepo(self._aptly, name)

    def get_publish(self) -> AptlyPublish:
        return AptlyPublish(self._aptly, self.version, self.endpoint)

    def get_snapshot(self) -> AptlySnapshot:
        name = f'v2:publish:{self.version}:publish:{self.timestamp.isoformat()}'
        return AptlySnapshot(self._aptly, name)

    def get_snapshot_temp(self, __count=[0]) -> AptlySnapshot:
        count = __count[0]
        __count[0] = count + 1
        name = f'v2:temp:publish:{self.version}:{self.timestamp.isoformat()}:{count}'
        ret = AptlySnapshot(self._aptly, name)
        self._snapshots_temp.add(ret)
        return ret

    def __call__(self) -> None:
        print_notice(f'Updating publish for version "{self.version}"')
        print_info(f'Using config "{self.config_file}"')

        self._snapshots_temp = set()

        config_publish = self.config['publish']
        config_sources = config_publish['sources']

        publish = self.get_publish()
        snapshot = self.get_snapshot()

        try:
            changed = False
            snapshots_merge: list[AptlySnapshot] = []

            for config_source in config_sources:
                if self._do_source(config_source, config_sources, snapshots_merge):
                    changed = True

            if changed or not publish.exist():
                print_notice(f'Creating snapshot "{snapshot.name}":')
                snapshot.create_merge(snapshots_merge, lambda la: print_output(la))
                print_notice(f'Updating publish "{publish.name}":')
                publish.update(snapshot, lambda la: print_output(la))

        except subprocess.CalledProcessError as e:
            print(e.stderr)
            raise

        finally:
            for snapshot in self._snapshots_temp:
                try:
                    snapshot.drop(force=True)
                except Exception:
                    pass

    def _do_source(self, config_source: dict, config_sources: list[dict], snapshots_merge: list[AptlySnapshot]) -> bool:
        source_type: str = config_source['type']

        if source_type == 'exclude':
            return False
        elif source_type == 'mirror':
            return self._do_source_mirror(config_source, config_sources, snapshots_merge)
        elif source_type == 'repo':
            return self._do_source_repo(config_source, config_sources, snapshots_merge)
        else:
            raise RuntimeError(f'Unsupported source "{source_type}"')

    def _do_source_mirror(self, config_source: dict, config_sources: list[dict], snapshots_merge: list[AptlySnapshot]) -> bool:
        '''
        Handle a source of type "mirror"
        '''
        mirror_name = config_source['name']
        snapshot_filter = self.get_snapshot_source_mirror_filter(mirror_name)
        snapshot_input = self.get_snapshot_source_mirror_input(mirror_name)
        snapshots_merge.append(snapshot_filter)

        if not snapshot_input.exist():
            raise RuntimeError(f'Snapshot "{snapshot_input.name}" for mirror "{mirror_name}" does not exist')

        packages_new = self.search(snapshot_input, config_source, config_sources)

        # Check contents of existing snapshot
        if snapshot_filter.exist():
            packages_old = snapshot_filter.search()

            if not self.use_force and packages_new == packages_old:
                print_notice(f'No change pulling from snapshot "{snapshot_input.name}"')
                return False

            snapshot_filter.drop(force=True)

        print_notice(f'Creating snapshot from snapshot "{snapshot_input.name}":')
        if packages_new:
            snapshot_filter.create_from_snapshot_filter(snapshot_input, list(packages_new), lambda la: print_output(la))
        else:
            snapshot_filter.create_empty(lambda la: print_output(la))

        return True

    def _do_source_repo(self, config_source: dict, config_sources: list[dict], snapshots_merge: list[AptlySnapshot]) -> bool:
        '''
        Handle a source of type "repo"
        '''
        repo = self.get_repo(config_source['name'])
        snapshot_filter = self.get_snapshot_source_repo_filter(repo.name)
        snapshots_merge.append(snapshot_filter)

        if not repo.exist():
            raise RuntimeError(f'Repo "{repo.name}" does not exist')

        packages_new = self.search(repo, config_source, config_sources)

        # Check contents of existing snapshot
        if snapshot_filter.exist():
            packages_old = snapshot_filter.search()

            if not self.use_force and packages_new == packages_old:
                print_notice(f'No change pulling from repo "{repo.name}"')
                return False

            snapshot_filter.drop(force=True)

        print_notice(f'Creating snapshot from repo "{repo.name}":')
        if packages_new:
            snapshot_temp = self.get_snapshot_temp()
            snapshot_temp.create_from_repo(repo, lambda la: print_output(la))
            snapshot_filter.create_from_snapshot_filter(snapshot_temp, list(packages_new), lambda la: print_output(la))
        else:
            snapshot_filter.create_empty(lambda la: print_output(la))

        return True


def main():
    logging.basicConfig()

    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        'config_file',
        help='use config file',
        metavar='CONFIG',
    )
    argparser.add_argument(
        'endpoint',
        metavar='ENDPOINT',
        type=str,
    )
    argparser.add_argument(
        '--force',
        action=argparse.BooleanOptionalAction,
        dest='use_force',
        default=False,
        help='re-create snapshots',
    )
    argparser.add_argument(
        '--version-override',
        help='override version provided in config',
    )
    args = argparser.parse_args()

    RepoPublishUpdate(**vars(args))()


if __name__ == '__main__':
    main()
