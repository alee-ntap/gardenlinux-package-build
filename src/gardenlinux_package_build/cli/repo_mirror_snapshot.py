# SPDX-License-Identifier: MIT

from __future__ import annotations

import argparse
import datetime
import logging
import subprocess
import yaml  # type: ignore[import]

from gardenlinux_package_build.aptly.lowlevel import AptlyEnv
from gardenlinux_package_build.aptly.mirror import AptlyMirror
from gardenlinux_package_build.aptly.snapshot import AptlySnapshot

from .utils import print_info, print_notice, print_output


class RepoMirrorSnapshot:
    config_file: str
    use_force: bool
    use_update: bool
    version: str
    config: dict
    _aptly: AptlyEnv

    def __init__(
        self, *,
        config_file: str,
        use_force: bool,
        use_update: bool,
        version_override: str | None = None,
    ) -> None:
        self.config_file = config_file
        self.use_force = use_force
        self.use_update = use_update
        self._aptly = AptlyEnv()

        with open(config_file) as f:
            self.config = yaml.safe_load(f)

        version: str = version_override or self.config['version']
        # XXX: Generic type checks
        if not isinstance(version, str):
            raise RuntimeError(f'Version "{version!r}" is not a string, but {type(version)}')
        if version == 'generate':
            # XXX: factor out
            version = str((datetime.date.today() - datetime.date.fromisoformat('2020-03-31')).days)
        self.version = version

    def get_mirror(self, name: str, dist: str, keyring: str) -> AptlyMirror:
        name = f'{name}:{dist}'
        return AptlyMirror(self._aptly, name, keyring)

    def get_snapshot(self, name: str, dist: str) -> AptlySnapshot:
        name = f'v2:mirror:{name}:{dist}:{self.version}'
        return AptlySnapshot(self._aptly, name)

    def _query(self, config_mirror: dict) -> str:
        packages: list = config_mirror.get('packages', [])

        # If no package filter is specified,
        # bail out because a filter should always
        # be specified.
        if len(packages) == 0:
            raise RuntimeError('Each mirror must have the packages attribute configured')

        query: list[str] = [self._query_one(i) for i in packages]

        return '|'.join(f'({i})' for i in query)

    def _get_flat_pkg_ver_list(self, pkg_list):
        if pkg_list is None:
            return None
        out = list()
        for item in pkg_list:
            if item is not None:
                if isinstance(item, dict):
                    out.append(next(iter(item)))
                else:
                    out.append(item)
        return out

    def _query_one(self, packages_match: dict) -> str:
        match_priorities = packages_match.get('matchPriorities')
        match_sources = self._get_flat_pkg_ver_list(packages_match.get('matchSources'))
        match_binaries = packages_match.get('matchBinaries')

        if not (match_priorities or match_sources or match_binaries):
            raise RuntimeError('Need at least on of matchPriorities, matchSources, matchBinaries')

        query: list[str] = []

        if match_priorities:
            query.append('|'.join(f'Priority ({i})' for i in match_priorities))
        if match_sources:
            query.append('|'.join(f'$Source (= {i})' for i in match_sources))
        if match_binaries:
            query.append('|'.join(f'{i}' for i in match_binaries))

        # Need to exclude sources here, so they are only included as dependency
        # and not as entry in our packages list
        return ','.join(f'(({i}), !$Architecture (= source))' for i in query)

    def __call__(self) -> None:
        print_notice(f'Updating mirror snapshots for version "{self.version}"')
        print_info(f'Using config "{self.config_file}"')

        try:
            for config_mirror in self.config.get('mirrors', []):
                self._do_mirror(config_mirror)
        except subprocess.CalledProcessError as e:
            print(e.stderr)
            raise

    def _do_mirror(self, config_mirror: dict) -> None:
        ''' Update one mirror configured in config '''

        for dist in config_mirror['dists']:
            self._do_dist(config_mirror, dist)

    def _do_dist(self, config_mirror: dict, dist: str) -> None:
        ''' Update one distribution configured in config '''

        name: str = config_mirror['name']
        url: str = config_mirror['url']
        keyring: str = config_mirror['keyring']
        arches: list[str] = config_mirror['arches']
        query = self._query(config_mirror)

        mirror = self.get_mirror(name, dist, keyring)
        mirror.create_edit(url, dist, arches, query, stdout=lambda la: print_output(la))
        if self.use_update:
            mirror.update(stdout=lambda la: print_output(la))

        snapshot = self.get_snapshot(name, dist)
        if snapshot.exist():
            if self.use_force:
                snapshot.drop(force=True)
            else:
                print_notice(f'Snapshot "{snapshot.name}" does already exist')
                return

        print_notice(f'Creating snapshot from snapshot "{snapshot.name}":')
        snapshot.create_from_mirror(mirror, stdout=lambda la: print_output(la))


def main():
    logging.basicConfig()

    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        'config_file',
        help='use config file',
        metavar='CONFIG',
    )
    argparser.add_argument(
        '--force',
        action=argparse.BooleanOptionalAction,
        dest='use_force',
        default=False,
        help='re-create snapshots',
    )
    argparser.add_argument(
        '--update',
        action=argparse.BooleanOptionalAction,
        dest='use_update',
        default=True,
        help='update mirror',
    )
    argparser.add_argument(
        '--version-override',
        help='override version provided in config',
    )
    args = argparser.parse_args()

    RepoMirrorSnapshot(**vars(args))()


if __name__ == '__main__':
    main()
