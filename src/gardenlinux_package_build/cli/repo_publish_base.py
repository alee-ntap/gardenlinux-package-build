# SPDX-License-Identifier: MIT

from __future__ import annotations

import datetime
import yaml  # type: ignore[import]

from gardenlinux_package_build.aptly.lowlevel import AptlyEnv
from gardenlinux_package_build.aptly.repo import AptlyRepo
from gardenlinux_package_build.aptly.snapshot import AptlySnapshot


class RepoPublishBase:
    config_file: str
    version: str
    config: dict
    _aptly: AptlyEnv

    def __init__(
        self, *,
        config_file: str,
        version_override: str | None = None,
    ) -> None:
        self.config_file = config_file
        self._aptly = AptlyEnv()

        with open(config_file) as f:
            self.config = yaml.safe_load(f)

        version: str = version_override or self.config['version']
        # XXX: Generic type checks
        if not isinstance(version, str):
            raise RuntimeError(f'Version "{version!r}" is not a string, but {type(version)}')
        if version == 'generate':
            version_major = (datetime.date.today() - datetime.date.fromisoformat("2020-03-31")).days
            version = f'{version_major}.0'
        self.version = version

    def get_snapshot_source_mirror_filter(self, name: str) -> AptlySnapshot:
        name = f'v2:publish:{self.version}:filter:mirror:{name}'
        return AptlySnapshot(self._aptly, name)

    def get_snapshot_source_mirror_input(self, name: str) -> AptlySnapshot:
        name = f'v2:mirror:{name}'
        return AptlySnapshot(self._aptly, name)

    def get_snapshot_source_repo_filter(self, name: str) -> AptlySnapshot:
        name = f'v2:publish:{self.version}:filter:repo:{name}'
        return AptlySnapshot(self._aptly, name)

    def search(self, source: AptlyRepo | AptlySnapshot, config_source: dict, config_sources: list[dict]) -> set[str]:
        packages: list | None = config_source.get('packages')
        if packages:
            return self.search_normal(source, packages)
        else:
            return self.search_remaining(source, config_sources)

    def search_normal(self, source: AptlyRepo | AptlySnapshot, packages: list[dict]) -> set[str]:
        result_pkgsource: set[str] = source.search([self._query_normal_pkgsource(packages)], only_latest=True)
        result_pkgbinary: set[str] = source.search([self._query_normal_pkgbinary(result_pkgsource)], only_latest=True)
        return result_pkgsource | result_pkgbinary

    def search_remaining(self, source: AptlyRepo | AptlySnapshot, config_sources: list[dict]) -> set[str]:
        return source.search([self._query_remaining(config_sources)])

    def _query_normal_pkgbinary(self, packages: set[str]) -> str:
        """
        Generate query to find all binary packages for a given set of aptly sources.
        """
        query: list[str] = []

        for package_str in packages:
            package: list[str] = package_str.split('_', 2)
            assert package[2] == 'source'

            query.extend((
                f'($Source (= {package[0]}), $SourceVersion (= {package[1]}))',
            ))

        return '|'.join(query)

    def _query_normal_pkgsource(self, packages: list[dict]) -> str:
        """
        Generate query to find all source packages for a given set of config entries.
        """
        query: list[str] = []

        for packages_match in packages:
            match_sources = packages_match.get('matchSources')

            if not match_sources:
                raise RuntimeError('Need matchSources')

            for package in match_sources:
                # If package definition contains list of annotations
                if isinstance(package, dict):
                    package = next(iter(package))
                package = package.split('/', 1)
                if len(package) == 2:
                    query.extend((
                        f'({package[0]}, $Architecture (= source), Version (= {package[1]}))',
                    ))
                else:
                    query.extend((
                        f'({package[0]}, $Architecture (= source))',
                    ))

        return '|'.join(query)

    def _query_remaining(self, config_sources: list[dict]) -> str:
        query: list[str] = [self._query_remaining_source(i) for i in config_sources if i]
        return ','.join(f'({i})' for i in query if i)

    def _query_remaining_source(self, config_source: dict) -> str:
        packages: list = config_source.get('packages', [])

        query: list[str] = []

        for packages_match in packages:
            match_binaries = packages_match.get('matchBinaries')
            match_sources = packages_match.get('matchSources')

            if match_binaries:
                for package in match_binaries:
                    # If package definition contains list of annotations
                    if isinstance(package, dict):
                        package = next(iter(package))
                    package = package.split('/', 1)
                    query.extend((
                        f'!({package[0]}, !$Architecture (= source))',
                    ))

            if match_sources:
                for package in match_sources:
                    # If package definition contains list of annotations
                    if isinstance(package, dict):
                        package = next(iter(package))
                    package = package.split('/', 1)
                    query.extend((
                        f'!({package[0]}, $Architecture (= source))',
                        f'!($Source (= {package[0]}))',
                    ))

        return ','.join(f'({i})' for i in query)
