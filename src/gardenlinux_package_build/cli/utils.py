# SPDX-License-Identifier: MIT

from __future__ import annotations

import enum
import typing
import sys


class Color(enum.IntEnum):
    ERROR = 9
    NOTICE = 10
    INFO = 2
    WARNING = 3
    OUTPUT = 6


def print_color(
    value: str,
    *,
    color: Color,
    end: str = '\n',
    file: typing.IO = sys.stdout,
    flush: bool = True,
):
    reset = '\033[39;49m'
    if color:
        begin = f'\033[38;5;{color}m'
    else:
        begin = reset
    print(begin, value, reset, sep='', end=end, file=file, flush=flush)


def print_error(
    value: str,
    *,
    end: str = '\n',
    file: typing.IO = sys.stdout,
    flush: bool = True,
) -> None:
    print_color(value, color=Color.ERROR, end=end, file=file, flush=flush)


def print_warning(
    value: str,
    *,
    end: str = '\n',
    file: typing.IO = sys.stdout,
    flush: bool = True,
) -> None:
    print_color(value, color=Color.WARNING, end=end, file=file, flush=flush)


def print_notice(
    value: str,
    *,
    end: str = '\n',
    file: typing.IO = sys.stdout,
    flush: bool = True,
) -> None:
    print_color(value, color=Color.NOTICE, end=end, file=file, flush=flush)


def print_info(
    value: str,
    *,
    end: str = '\n',
    file: typing.IO = sys.stdout,
    flush: bool = True,
) -> None:
    print_color(value, color=Color.INFO, end=end, file=file, flush=flush)


def print_output(
    value: str,
    *,
    end: str = '\n',
    file: typing.IO = sys.stdout,
    flush: bool = True,
) -> None:
    print_color(value, color=Color.OUTPUT, end=end, file=file, flush=flush)
