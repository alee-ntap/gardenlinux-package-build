# SPDX-License-Identifier: MIT

from __future__ import annotations

import argparse
import difflib
import logging
import subprocess
import typing

from gardenlinux_package_build.aptly.repo import AptlyRepo

from .repo_publish_base import RepoPublishBase
from .utils import print_error, print_info, print_notice, print_output


class RepoPublishDiff(RepoPublishBase):
    def get_repo(self, name: str) -> AptlyRepo:
        return AptlyRepo(self._aptly, name)

    def diff_packages(self, old: set[str], new: set[str], fromfile: str = 'Old', tofile: str = 'New') -> typing.Iterable[str]:
        return list(difflib.unified_diff(
            sorted(old),
            sorted(new),
            fromfile=fromfile,
            tofile=tofile,
            lineterm='',
        ))

    def __call__(self) -> None:
        print_notice(f'Diff publish for version "{self.version}"')
        print_info(f'Using config "{self.config_file}"')

        config_publish = self.config['publish']
        config_sources = config_publish['sources']

        try:
            for config_source in config_sources:
                self._do_source(config_source, config_sources)

        except subprocess.CalledProcessError as e:
            print(e.stderr)
            raise

    def _do_source(self, config_source: dict, config_sources: list[dict]) -> None:
        source_type: str = config_source['type']

        if source_type == 'exclude':
            pass
        elif source_type == 'mirror':
            self._do_source_mirror(config_source, config_sources)
        elif source_type == 'repo':
            self._do_source_repo(config_source, config_sources)
        else:
            raise RuntimeError(f'Unsupported source "{source_type}"')

    def _do_source_mirror(self, config_source: dict, config_sources: list[dict]) -> None:
        '''
        Handle a source of type "mirror"
        '''
        mirror_name = config_source['name']
        snapshot_filter = self.get_snapshot_source_mirror_filter(mirror_name)
        snapshot_input = self.get_snapshot_source_mirror_input(mirror_name)

        if not snapshot_input.exist():
            print_error(f'Snapshot "{snapshot_input.name}" for mirror "{mirror_name}" does not exist')
            return

        packages_new = self.search(snapshot_input, config_source, config_sources)

        # Check contents of existing snapshot
        if snapshot_filter.exist():
            packages_old = snapshot_filter.search()
            diff = self.diff_packages(packages_old, packages_new)
        else:
            packages_old = set()
            diff = self.diff_packages(packages_old, packages_new, fromfile='Missing')

        if diff:
            print_notice(f'Changes pulling from snapshot "{snapshot_input.name}"')
            for line in diff:
                print_output(line)
        else:
            if packages_old:
                print_notice(f'No change pulling from snapshot "{snapshot_input.name}"')
            else:
                print_error(f'Empty package list pulling from snapshot "{snapshot_input.name}"')

    def _do_source_repo(self, config_source: dict, config_sources: list[dict]) -> None:
        '''
        Handle a source of type "repo"
        '''
        repo = self.get_repo(config_source['name'])
        snapshot_filter = self.get_snapshot_source_repo_filter(repo.name)

        if not repo.exist():
            print_error(f'Repo "{repo.name}" does not exist')
            return

        packages_new = self.search(repo, config_source, config_sources)

        # Check contents of existing snapshot
        if snapshot_filter.exist():
            packages_old = snapshot_filter.search()
            diff = self.diff_packages(packages_old, packages_new)
        else:
            packages_old = set()
            diff = self.diff_packages(packages_old, packages_new, fromfile='Missing')

        if diff:
            print_notice(f'Changes pulling from repo "{repo.name}"')
            for line in diff:
                print_output(line)
        else:
            if packages_old:
                print_notice(f'No change pulling from repo "{repo.name}"')
            else:
                print_error(f'Empty package list pulling from repo "{repo.name}"')


def main():
    logging.basicConfig()

    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        'config_file',
        help='use config file',
        metavar='CONFIG',
    )
    argparser.add_argument(
        '--version-override',
        help='override version provided in config',
    )
    args = argparser.parse_args()

    RepoPublishDiff(**vars(args))()


if __name__ == '__main__':
    main()
