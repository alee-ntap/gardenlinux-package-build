# SPDX-License-Identifier: MIT

from __future__ import annotations

import subprocess
import typing

from .lowlevel import AptlyEnv
from .mirror import AptlyMirror
from .repo import AptlyRepo


CallableStdout = typing.Optional[typing.Callable[[str], None]]


class AptlySnapshot:
    _env: AptlyEnv
    name: str

    def __init__(self, env: AptlyEnv, name: str) -> None:
        self._env = env
        self.name = name

    def _read_stdout(
        self,
        stdout: CallableStdout,
        proc: subprocess.CompletedProcess,
    ) -> None:
        if stdout:
            for line in proc.stdout.split('\n'):
                if line:
                    stdout(line)

    def create_empty(
        self,
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.snapshot_create_empty(
            self.name,
        ))

    def create_from_mirror(
        self,
        mirror: AptlyMirror,
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.snapshot_create_from_mirror(
            self.name,
            mirror.name,
        ))

    def create_from_repo(
        self,
        repo: AptlyRepo,
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.snapshot_create_from_repo(
            self.name,
            repo.name,
        ))

    def create_from_snapshot_filter(
        self,
        snapshot: AptlySnapshot,
        query: list[str],
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.snapshot_filter(
            self.name,
            snapshot.name,
            query,
        ))

    def create_merge(
        self,
        snapshots: typing.Iterable[AptlySnapshot],
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.snapshot_merge(
            self.name,
            [snapshot.name for snapshot in snapshots],
        ))

    def drop(
        self,
        force: bool = False,
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.snapshot_drop(
            self.name,
            force,
        ))

    def exist(self) -> bool:
        return self._env.snapshot_exist(self.name)

    def search(
        self,
        query: list[str] = [],
        *,
        only_latest: bool = False,
    ) -> set[str]:
        args = [
            '-dep-follow-source',
        ]

        return self._env.snapshot_search(self.name, args, query, only_latest)
