# SPDX-License-Identifier: MIT

from __future__ import annotations

import pathlib
import subprocess


class AptlyEnv:
    def __init__(self, config: pathlib.Path | None = None):
        self._config = config

    def _subprocess_run(self, args: list[str]) -> subprocess.CompletedProcess:
        cmd = ['aptly']
        if self._config:
            cmd.extend(('-config', str(self._config)))
        cmd.extend(args)
        return subprocess.run(
            cmd,
            check=True,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )

    def _impl_exist(self, obj: str, args: list[str]) -> bool:
        try:
            self._subprocess_run([obj, 'show'] + args)
            return True
        except subprocess.CalledProcessError as e:
            if e.returncode == 1:
                # XXX: Sometimes aptly might error our even if the object
                # exists, for example if a source snapshot is missing.
                # Just check if the output shows something usable.
                if obj == 'snapshot' and e.stdout.startswith(f'Name: {args[0]}'):
                    return True
                return False
            raise

    def _impl_search(self, obj: str, name: str, args: list[str], query: list[str], only_latest: bool) -> set[str]:
        ret: set[str] = set()
        try:
            proc = self._subprocess_run([obj, 'search'] + args + [name] + query)
        except subprocess.CalledProcessError as e:
            # XXX
            if e.returncode == 1 and e.stderr.strip() == 'ERROR: no results':
                return ret
            raise
        seen = set()
        # Search output of aptly is sorted by version, newest first.
        # So remove all later occurances
        for line in proc.stdout.split('\n'):
            if not line:
                continue
            if not only_latest:
                ret.add(line)
            else:
                il = line.split('_', 2)
                key = f'{il[0]}_{il[2]}'
                if key not in seen:
                    ret.add(line)
                    seen.add(key)
        return ret

    def mirror_create(self, name: str, url: str, dist: str, args: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['mirror', 'create'] + args + [name, url, dist])

    def mirror_edit(self, name: str, url: str, args: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['mirror', 'edit'] + args + [f'-archive-url={url}', name])

    def mirror_exist(self, name: str) -> bool:
        return self._impl_exist('mirror', [name])

    def mirror_search(self, name: str, args: list[str], query: list[str], only_latest: bool) -> set[str]:
        return self._impl_search('mirror', name, args, query, only_latest)

    def mirror_update(self, name: str, args: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['mirror', 'update'] + args + [name])

    def snapshot_create_empty(self, name: str) -> subprocess.CompletedProcess:
        return self._subprocess_run(['snapshot', 'create', name, 'empty'])

    def snapshot_create_from_mirror(self, name: str, mirror: str) -> subprocess.CompletedProcess:
        return self._subprocess_run(['snapshot', 'create', name, 'from', 'mirror', mirror])

    def snapshot_create_from_repo(self, name: str, repo: str) -> subprocess.CompletedProcess:
        return self._subprocess_run(['snapshot', 'create', name, 'from', 'repo', repo])

    def snapshot_drop(self, name: str, force: bool) -> subprocess.CompletedProcess:
        if force:
            return self._subprocess_run(['snapshot', 'drop', '-force', name])
        else:
            return self._subprocess_run(['snapshot', 'drop', name])

    def snapshot_exist(self, name: str) -> bool:
        return self._impl_exist('snapshot', [name])

    def snapshot_filter(self, name: str, source: str, query: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['snapshot', 'filter', source, name] + query)

    def snapshot_merge(self, name: str, sources: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['snapshot', 'merge', '-latest', name] + sources)

    def snapshot_search(self, name: str, args: list[str], query: list[str], only_latest: bool) -> set[str]:
        return self._impl_search('snapshot', name, args, query, only_latest)

    def publish_exist(self, name: str, endpoint: str) -> bool:
        return self._impl_exist('publish', [name, endpoint])

    def publish_snapshot(self, name: str, endpoint: str, snapshot: str, args: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['publish', 'snapshot'] + args + [f'-distribution={name}', snapshot, endpoint])

    def publish_switch(self, name: str, endpoint: str, snapshot: str, args: list[str]) -> subprocess.CompletedProcess:
        return self._subprocess_run(['publish', 'switch'] + args + [name, endpoint, snapshot])

    def repo_exist(self, name: str) -> bool:
        return self._impl_exist('repo', [name])

    def repo_search(self, name: str, args: list[str], query: list[str], only_latest: bool) -> set[str]:
        return self._impl_search('repo', name, args, query, only_latest)
