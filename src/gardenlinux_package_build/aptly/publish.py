# SPDX-License-Identifier: MIT

from __future__ import annotations

import subprocess
import typing

from .lowlevel import AptlyEnv
from .snapshot import AptlySnapshot


CallableStdout = typing.Optional[typing.Callable[[str], None]]


class AptlyPublish:
    _env: AptlyEnv
    name: str
    endpoint: str

    def __init__(self, env: AptlyEnv, name: str, endpoint: str) -> None:
        self._env = env
        self.name = name
        self.endpoint = endpoint

    def _read_stdout(
        self,
        stdout: CallableStdout,
        proc: subprocess.CompletedProcess,
    ) -> None:
        if stdout:
            for line in proc.stdout.split('\n'):
                if line:
                    stdout(line)

    def exist(self) -> bool:
        return self._env.publish_exist(self.name, self.endpoint)

    def update(
        self,
        snapshot: AptlySnapshot,
        stdout: CallableStdout = None,
    ) -> None:
        if not self.exist():
            self._read_stdout(stdout, self._env.publish_snapshot(
                self.name,
                self.endpoint,
                snapshot.name,
                [
                    '-batch',
                    '-skip-contents',
                    '-origin=GardenLinux',
                    '-acquire-by-hash',
                    '-butautomaticupgrades=yes',
                    '-notautomatic=yes',
                ],
            ))
        else:
            self._read_stdout(stdout, self._env.publish_switch(
                self.name,
                self.endpoint,
                snapshot.name,
                [
                    '-batch',
                    '-skip-contents',
                ],
            ))
