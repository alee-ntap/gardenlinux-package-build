# SPDX-License-Identifier: MIT

from __future__ import annotations

import subprocess
import typing

from .lowlevel import AptlyEnv


CallableStdout = typing.Optional[typing.Callable[[str], None]]


class AptlyRepo:
    _env: AptlyEnv
    name: str

    def __init__(self, env: AptlyEnv, name: str) -> None:
        self._env = env
        self.name = name

    def _read_stdout(
        self,
        stdout: CallableStdout,
        proc: subprocess.CompletedProcess,
    ) -> None:
        if stdout:
            for line in proc.stdout.split('\n'):
                if line:
                    stdout(line)

    def exist(self) -> bool:
        return self._env.repo_exist(self.name)

    def search(
        self,
        query: list[str] = [],
        *,
        only_latest: bool = False,
    ) -> set[str]:
        args = [
            '-dep-follow-source',
        ]

        return self._env.repo_search(self.name, args, query, only_latest)
