# SPDX-License-Identifier: MIT

from __future__ import annotations

import subprocess
import typing

from .lowlevel import AptlyEnv


CallableStdout = typing.Optional[typing.Callable[[str], None]]


class AptlyMirror:
    _env: AptlyEnv
    name: str
    keyring: str

    def __init__(
        self,
        env: AptlyEnv,
        name: str,
        keyring: str,
    ) -> None:
        self._env = env
        self.name = name
        self.keyring = keyring

    def _read_stdout(
        self,
        stdout: CallableStdout,
        proc: subprocess.CompletedProcess,
    ) -> None:
        if stdout:
            for line in proc.stdout.split('\n'):
                if line:
                    stdout(line)

    def create_edit(
        self,
        url: str,
        dist: str,
        arches: list[str],
        query: str,
        stdout: CallableStdout = None,
    ) -> None:
        args = [
            f'-architectures={",".join(arches)}',
            f'-filter={query}',
            '-filter-with-deps',
            f'-keyring={self.keyring}',
            '-with-sources',
        ]

        if not self._env.mirror_exist(self.name):
            self._read_stdout(stdout, self._env.mirror_create(self.name, url, dist, args))

        self._read_stdout(stdout, self._env.mirror_edit(self.name, url, args))

    def search(
        self,
        query: list[str] = [],
        *,
        only_latest: bool = False,
    ) -> set[str]:
        args = [
            '-dep-follow-source',
        ]

        return self._env.mirror_search(self.name, args, query, only_latest)

    def update(
        self,
        stdout: CallableStdout = None,
    ) -> None:
        self._read_stdout(stdout, self._env.mirror_update(
            self.name,
            [
                '-dep-follow-source',
                f'-keyring={self.keyring}',
            ]
        ))
