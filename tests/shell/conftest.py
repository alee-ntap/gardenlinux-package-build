# SPDX-License-Identifier: MIT

import json
import pathlib
import pytest
import subprocess
import sys


class Aptly:
    def __init__(self, tmp_path):
        self.tmp_path = tmp_path

        self.config_path = tmp_path / '.aptly.conf'
        self.repo_path = tmp_path / '.aptly'

        self.run_env = {
            'HOME': str(tmp_path),
            'PATH': '/usr/bin:/bin',
        }

        with self.config_path.open('w') as f:
            json.dump({
                'architectures': ['amd64', 'arm64'],
                'gpgDisableSign': True,
            }, f)

    def _run(self, command, args, stderr=subprocess.PIPE):
        cmd = [command] + args
        proc = subprocess.run(
            cmd,
            cwd=str(self.tmp_path),
            env=self.run_env,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=stderr,
            text=True,
        )
        if proc.stdout:
            sys.stdout.write(f'## Output from command {cmd}\n')
            sys.stdout.write(proc.stdout)
            sys.stdout.write('## End\n\n')
        if proc.stderr:
            sys.stderr.write(f'## Output from command {cmd}\n')
            sys.stderr.write(proc.stderr)
            sys.stderr.write('## End\n\n')
        if proc.returncode:
            raise subprocess.CalledProcessError(
                proc.returncode,
                proc.args,
                output=proc.stdout,
                stderr=proc.stderr,
            )
        return proc

    def _run_aptly(self, args):
        run = self._run('aptly', args)
        return set(i for i in run.stdout.split('\n') if i != '')

    def mirror_list(self):
        return self._run_aptly(['mirror', 'list', '-raw'])

    def mirror_search(self, mirror, *args):
        return self._run_aptly(['mirror', 'search', mirror] + list(args))

    def publish_list(self):
        return self._run_aptly(['publish', 'list', '-raw'])

    def repo_create(self, repo):
        return self._run_aptly(['repo', 'create', repo])

    def snapshot_create_empty(self, snapshot):
        return self._run_aptly(['snapshot', 'create', snapshot, 'empty'])

    def snapshot_list(self):
        return self._run_aptly(['snapshot', 'list', '-raw'])

    def snapshot_search(self, snapshot, *args):
        return self._run_aptly(['snapshot', 'search', snapshot] + list(args))


@pytest.fixture
def aptly(tmp_path):
    return Aptly(tmp_path)


@pytest.fixture
def root_path(pytestconfig):
    return pathlib.Path(pytestconfig.rootpath)


@pytest.fixture
def run(aptly, root_path):
    bindir = root_path / 'bin'

    def func(script, *args):
        return aptly._run(bindir / script, list(args), stderr=subprocess.STDOUT)

    return func


@pytest.fixture
def run_system(aptly):
    def func(script, *args):
        return aptly._run(script, list(args), stderr=subprocess.STDOUT)

    return func
