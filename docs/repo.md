# Garden Linux package repository 
The `repo.gardenlinux.io` is an APT repository providing packages for Garden Linux. It is structured by daily releases, matching the Garden Linux versioning convention. Versioning of Garden Linux is explained in the [VERSION.md](../VERSION.md) of the Garden Linux main repository.

Packages from a given daily release will be kept (historic releases) to provide correct package versions for all Garden Linux releases since version `685`. The most up-to-date release can be found in `today`, which always points to today's release. What packages are part of which distro (`685.0`, `686.0`, `today`, ...) are defined in so called package configurations. Take a look into the [Garden Linux repository suites](#garden-linux-repository-suites) chapter for more details.

The following sections explain the typical lifecycle of a Garden Linux package, starting from its sources to the actual release into the `repo.gardenlinux.io` repository.

- [Garden Linux package repository](#garden-linux-package-repository)
- [Garden Linux repository suites](#garden-linux-repository-suites)
- [Package Pipelines](#package-pipelines)
  - [Creation of a Garden Linux package](#creation-of-a-garden-linux-package)
    - [Mirror a package from debian](#mirror-a-package-from-debian)
    - [Create a package from Debian sources](#create-a-package-from-debian-sources)
    - [Create a package from a custom source uploaded to pristine-lfs](#create-a-package-from-a-custom-source-uploaded-to-pristine-lfs)
    - [Create a package from a git sources](#create-a-package-from-git-sources)
    - [Create a package from scratch](#create-a-package-from-scratch)
  - [Package upload repo.gardenlinux.io](#package-upload-repogardenlinuxio)


# Garden Linux repository suites

The suite `today` and the `major.0` suites are created daily. Patch releases `major.minor` with minor > 0 must be created manually. Please find a detailed documentation [here](../packages/README.md).


# Package Pipelines
Multiple stages must be passed before a package can be deployed to the `repo.gardenlinux.io` repository.
The logical stages are:

- Create a source package:
  - Default pipeline: [source.yml](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/blob/main/pipeline/source.yml).
- Create a binary package:
  - Default pipeline: [build.yml](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/blob/main/pipeline/build.yml).
- Sign the source package:
  - Default pipeline: [signed-source.yml](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/blob/main/pipeline/signed-source.yml).
  - Only runs if `BUILD_SIGNED` is defined in `.gitlab-ci.yml`.
- Sign the binary package:
  - Default pipeline: [signed-build.yml](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/blob/main/pipeline/signed-build.yml).
  - Only runs if `BUILD_SIGNED` is defined in `.gitlab-ci.yml`.
- Test:
  - Default pipeline: [test.yml](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/blob/main/pipeline/test.yml).

The [gardenlinux-package-build](https://gitlab.com/gardenlinux/gardenlinux-package-build/) repository contains [default pipelines](../pipeline/) that can be used to define the logical steps mentioned above. A developer does not need to define these stages on its own each time whenever a new package must be built for Garden Linux but can use these predefined pipelines to systematically create Garden Linux packages.

However, in some cases it is necessary to use a customized pipeline instead of the default equivalent. Which pipeline to use is controlled by the `.gitlab-ci.yml` file in the actual Garden Linux package repository which can either reference the default pipelines of the [gardenlinux-package-build](../pipeline/) repo or reference locally defined custom pipelines of the repository itself.

The following simplified example shows the usage of a custom local `source` stage. 

```yml
stages:
- source
- build

# Source pipeline from package repo in .gitlab/ci/source.yml
# Build pipeline from central gardenlinux-package-build repo (this)
include:
- local: .gitlab/ci/source.yml
- project: gardenlinux/gardenlinux-package-build
  file:
  - pipeline/build.yml
```

As an example, the [linux-5.10 project](https://gitlab.com/gardenlinux/gardenlinux-package-linux-5.10/-/tree/main/) uses a local pipeline version of the source stage, see [source.yml](https://gitlab.com/gardenlinux/gardenlinux-package-linux-5.10/-/blob/main/.gitlab/ci/source.yml) and [.gitlab-ci.yml](https://gitlab.com/gardenlinux/gardenlinux-package-linux-5.10/-/blob/main/.gitlab-ci.yml).


## Creation of a Garden Linux package
From least to most effort required, the following sections describe methods to create a package for `repo.gardenlinux.io`. 
The upload to the APT repository is handled by scheduled pipelines from the [gardenlinux-package-build](https://gitlab.com/gardenlinux/gardenlinux-package-build/) project, which is described in [Package upload repo.gardenlinux.io](#package-upload-repogardenlinuxio).

### Mirror a package from debian

* Add name of debian package to mirror the package configuration [today.yaml](../packages/today.yaml). What can be configured there, is described in more detail [here](../packages/README.md).

### Create a package from Debian sources

* Create a Gitlab Repository.
* Create `.gitlab-ci.yml`.
    * required variable `SOURCE_NAME`: downloads sources from the defined source and creates a source package.
  
Simple example: [gardenlinux-package-tcpdump](https://gitlab.com/gardenlinux/gardenlinux-package-tcpdump/-/blob/main/.gitlab-ci.yml)

### Create a package from a custom source uploaded to pristine-lfs 

* Create a Gitlab Repository.
* Create `.gitlab-ci.yml`.
   * required variable `ORIGTAR`: **Name** of origtar uploaded to branch named `pristine-lfs` via pristine-lfs. 
   * `ORIGTAR`.
* Setup pristine-lfs.
   * `git checkout --orphan pristine-lfs`.
   * `git lfs install`.
   * Create a `.gitattributes`.
```
*.tar.* filter=lfs diff=lfs merge=lfs -text
*.tar.*.asc -filter -diff merge=binary -text
*.dsc -filter !diff merge=binary !text
```
* prepare the original upstream sources and upload a orig.tar.{xz,gz} file to `pristine-lfs` branch.
```
# Simplified example for preparing upstream orig.tar.xz file
git clone --quiet --depth 1 --branch "$REPO_REF" "$REPO_URL" "${SOURCE_NAME}-${UPSTREAM_VERSION}"    
tar -cJf "${SOURCE_NAME}_${UPSTREAM_VERSION}.orig.tar.xz" "${SOURCE_NAME}-${UPSTREAM_VERSION}"
pristine-lfs commit "${SOURCE_NAME}_${UPSTREAM_VERSION}.orig.tar.xz"
```

Simple example: [gardenlinux-package-runc](https://gitlab.com/gardenlinux/legacy-packages/gardenlinux-package-runc)

### Create a package from git sources

* Create a Gitlab Repository.
* Create `.gitlab-ci.yml`.
  * Include the `pipeline/pipeline-git.yml` pipeline.
  * `SOURCE_NAME`: The name of the source package.
  * `SOURCE_REPO`: The URL to the git repository.
  * `SOURCE_TAG_PREFIX` (_optional_): Define a different git tag prefix if the git tag do not follow the "v\<version number\>" (e.g v1.0.0) schema.
* Create a `debian/` folder.
  * Add all required debian files in order to create the package.
  * If the upstream repository already contains a debian directory, it gets removed.

The following file shows an example for the `.gitlab-ci.yml`:
```yml
include:
  project: gardenlinux/gardenlinux-package-build
  file: pipeline/pipeline-git.yml

variables:
  SOURCE_NAME: aws-sdk-cpp
  SOURCE_REPO: https://github.com/aws/aws-sdk-cpp.git
  SOURCE_TAG_PREFIX: ''
```

Simple example: [gardenlinux-package-aws-kms-pkcs11](https://gitlab.com/gardenlinux/gardenlinux-package-aws-kms-pkcs11)

### Create a package from scratch

* Create a Gitlab Repository.
* Create `.gitlab-ci.yml`.
  * Include the `pipeline/pipeline-native.yml` pipeline.
* Upload your sources to the gitlab repo and maintain them here.
* Create a `debian/` folder.
* Create a custom source pipeline.
   * Output of source stage: `*.dsc`, `*.changes` and a `*.tar.*` file.
   * archive output, see below.

```yml
  artifacts:
    paths:
    - _output/*.changes
    - _output/*.dsc
    - _output/*.tar.*
    expire_in: 1 week
```

Simple example: [gardenlinux-package-gardenlinux-selinux-module](https://gitlab.com/gardenlinux/gardenlinux-package-gardenlinux-selinux-module)

## Package upload repo.gardenlinux.io
Each day, a scheduled gitlab pipeline is triggered, see [Gitlab pipeline schedules](https://gitlab.com/gardenlinux/gardenlinux-package-build/-/pipeline_schedules).

The `driver/run` helper orchestrates which `bin/garden-repo-*` helper to invoke, and the helper scripts are using `aptly`. Packages are uploaded to a S3 bucket, which is exposed via repo.gardenlinux.io to serve as the APT repository for Garden Linux.

