#!/bin/bash
# SPDX-License-Identifier: MIT

set -euE

SIGNING_KEY_FILE=$1
INPUT_DIR=$2
OUTPUT_DIR=$3

TMP_DIR=${OUTPUT_DIR}/tmp
TMP_P12=${TMP_DIR}/sign.p12

mkdir -p "$TMP_DIR"
echo '### create nss cert store'
openssl pkcs12 -export -out "$TMP_P12" -inkey "$SIGNING_KEY_FILE" -in "$SIGNING_KEY_FILE" -passout pass:"" -name sign
pk12util -i "$TMP_P12" -d "$TMP_DIR" -W "" -K "" >/dev/null

for template in "$INPUT_DIR"/*-signed-template_*_*.deb; do
  template_deb=$(basename "$template")
  template_name=${template_deb%%_*}
  template_dir="${OUTPUT_DIR}/${template_name}"
  template_tmpdir="${TMP_DIR}/${template_name}"
  template_json="${TMP_DIR}/${template_name}.json"

  echo "### extracting template ${template_name}"
  dpkg-deb -x "$template" "$template_tmpdir"
  mv "${template_tmpdir}/usr/share/code-signing/${template_name}/files.json" "$template_json"
  mv "${template_tmpdir}/usr/share/code-signing/${template_name}/source-template" "$template_dir"

  for unsigned_name in $(jq -j '.packages | keys | join(" ")' < "$template_json"); do
    unsigned_dir="${TMP_DIR}/${unsigned_name}"

    echo "### extracting unsigned package ${unsigned_name}"
    dpkg-deb -x "${INPUT_DIR}/${unsigned_name}"_*_*.deb "$unsigned_dir"

    echo "### signing package ${unsigned_name}"
    while read -r file sigtype; do
      filein="${unsigned_dir}/${file}"
      fileout="${template_dir}/debian/signatures/${unsigned_name}/${file}.sig"
      dirout=$(dirname "$fileout")

      mkdir -p "$dirout"
      if [[ $sigtype = efi ]]; then
        pesign -i "$filein" --export-signature "$fileout" --sign -d sha256 -n "$TMP_DIR" -c sign
      elif [[ $sigtype = linux-module ]]; then
        openssl cms -sign -binary -outform DER -signer "$SIGNING_KEY_FILE" -md sha256 -nocerts -noattr -nosmimecap -in "$filein" -out "$fileout"
      else
        echo "ERROR: unknown signing type ${sigtype}"
        exit 1
      fi
    done < <(jq -j ".packages[\"${unsigned_name}\"].files[] | [.file, .sig_type] | join(\" \") + \"\n\"" < "$template_json")
  done

  echo "### building source package ${template_name}"
  ( cd "$template_dir"; dpkg-buildpackage -us -uc -S -nc -d )
done
