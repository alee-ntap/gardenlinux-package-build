# SPDX-License-Identifier: MIT
# shellcheck shell=bash

TIMESTAMP=$(TZ=UTC date -d "${1}" '+%s')
VERSION_TODAY_DATE=$(python3 -c 'from datetime import *; from sys import argv; days = (date.fromtimestamp(int(argv[1])) - date.fromisoformat("2020-03-31")).days; print(f"{days}")' "$TIMESTAMP")
# shellcheck disable=SC2034
VERSION_TODAY=${VERSION_TODAY_DATE}.0

NAME_SNAPSHOT_BACKUP_PREFIX="v1:backup:${TIMESTAMP}"

declare -A _COLOR=(
  [bold_red]="\033[31;1m"
  [bold_green]="\033[32;1m"
  [yellow]="\033[0;33m"
  [reset]="\033[0;m"
)

function error() {
  echo -e "${_COLOR[bold_red]}ERROR: $*${_COLOR[reset]}" >&2
  exit 1
}

function warning() {
  echo -e "${_COLOR[yellow]}WARNING: $*${_COLOR[reset]}" >&2
}

function notice() {
  echo -e "${_COLOR[bold_green]}$*${_COLOR[reset]}"
}

function packages_file() {
  local file="${PACKAGES}/${1}"
  if [[ -f $file ]]; then
    sed -E -e '/^\s*(#|$)/d' "$file"
  else
    error "File $file not found"
  fi
}

function packages_query_exclude_baseos() {
  local dir=$1
  local name_query=$2
  local name_source=$3
  local -a e

  while IFS=" " read -r query; do
    e+=("!${query}")
  done < <(packages_file "${dir}/${name_query}")

  while IFS=" " read -r package version; do
    e+=("!(${package}, \$Architecture (= source))")
    e+=("!\$Source (= ${package})")
  done < <(packages_file "${dir}/${name_source}")

  {
    # "," means AND
    local IFS=','
    echo -n "${e[*]}"
  }
}

function packages_query_include() {
  local name="$1"
  local -a e

  while IFS=" " read -r package; do
    e+=("${package}")
  done < <(packages_file "${name}-include")

  {
    # "|" means OR
    local IFS='|'
    echo -n "${e[*]}"
  }
}

function packages_query_include_source() {
  local name="$1"
  local -a e

  while IFS=" " read -r package version; do
    if [[ $version ]]; then
      e+=("${package}, \$Architecture (= source), Version (= ${version})")
      e+=("\$Source (= ${package}), \$SourceVersion (= ${version})")
    else
      e+=("${package}, \$Architecture (= source)")
      e+=("\$Source (= ${package})")
    fi
  done < <(packages_file "${name}-include")

  {
    # "|" means OR
    local IFS='|'
    echo -n "${e[*]}"
  }
}

function name_snapshot_baseos() {
  local version="$1"
  local mirror="$2"

  echo "v1:baseos:${version%%.*}:${mirror}"
}

function name_snapshot_publish_baseos() {
  local version="$1"
  local mirror="$2"

  echo "v1:publish:${version}:0:${mirror}"
}

function name_snapshot_publish_gardenlinux_all() {
  local version="$1"

  echo "v1:publish:${version}:1:gardenlinux:all"
}

function name_snapshot_publish_gardenlinux() {
  local version="$1"

  echo "v1:publish:${version}:2:gardenlinux"
}

function name_snapshot_publish_final() {
  local version="$1"

  echo "v1:publish:${version}:3:publish"
}

function aptly_mirror_exist {
  local name="$1"

  if aptly mirror list -raw | grep -Fx "$name" > /dev/null; then
    return 0
  fi

  return 1
}

function aptly_publish_exist {
  local name="$1"

  if aptly publish list -raw | grep -Fx "$name" > /dev/null; then
    return 0
  fi

  return 1
}

function aptly_repo_exist {
  local name="$1"

  if aptly repo list -raw | grep -Fx "$name" > /dev/null; then
    return 0
  fi

  return 1
}

function aptly_snapshot_backup {
  local name="$1"

  aptly snapshot rename "$name" "${NAME_SNAPSHOT_BACKUP_PREFIX}:${name}" 2>/dev/null
}

function aptly_snapshot_changed {
  local -a content_exist
  local name="$1"
  local -n content_new_ref="$2"

  mapfile -t content_exist < <(aptly snapshot search "$name")

  if [[ ${content_exist[*]} == "${content_new_ref[*]}" ]]; then
    return 0
  fi

  return 1
}

function aptly_snapshot_exist {
  local name="$1"

  if aptly snapshot list -raw | grep -Fx "$name" > /dev/null; then
    return 0
  fi

  return 1
}

function aptly_snapshot_filter {
  local name_source="$1"
  local name_dest="$2"
  shift 2

  if [[ $# -gt 0 ]]; then
    aptly snapshot filter \
      "$name_source" \
      "$name_dest" \
      "$@"
  else
    aptly snapshot create \
      "$name_dest" \
      empty
  fi
}
